<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class SampleValidation extends TestCase
{
    private $url;

    protected function setUp(): void
    {
        parent::setUp();
        $this->url = "/api/data";
    }

    public function testIfNoInputProvidedReturnError()
    {
        $response = $this->json("post", $this->url, []);

        $response->assertStatus(422);
    }
}

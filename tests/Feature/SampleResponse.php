<?php

namespace Tests\Feature;

use Tests\TestCase;

class SampleResponse extends TestCase
{
    private $url;

    protected function setUp(): void
    {
        parent::setUp();
        $this->url = "/api/data";
    }

    public function testIfOnlyOneInputReturnThatOne()
    {
        $data = ["firstInput" => "hi"];

        $response = $this->json("post", $this->url, $data);

        $response->assertStatus(200);
        $this->assertEquals($response->getContent(), '{"data":{"result":"hi"}}');
    }

    public function testIfBothIntegerInputsReturnTheirSum()
    {
        $data = ["firstInput" => 3, "secondInput" => 2];

        $response = $this->json("post", $this->url, $data);

        $response->assertStatus(200);
        $this->assertEquals($response->getContent(), '{"data":{"result":5}}');
    }

    public function testIfEitherInputIsStringReturnInputsConcatenation()
    {
        $data = ["firstInput" => "hi", "secondInput" => 2];

        $response = $this->json("post", $this->url, $data);

        $response->assertStatus(200);
        $this->assertEquals($response->getContent(), '{"data":{"result":"hi2"}}');
    }

    public function testIfBothInputsAreStringReturnInputsConcatenation()
    {
        $data = ["firstInput" => "hi", "secondInput" => "Razi"];

        $response = $this->json("post", $this->url, $data);

        $response->assertStatus(200);
        $this->assertEquals($response->getContent(), '{"data":{"result":"hiRazi"}}');
    }

    public function testIfEitherInputIsEmojiReturnThatEmoji()
    {
        $data = ["firstInput" => "🙂", "secondInput" => 2];

        $response = $this->json("post", $this->url, $data);

        $response->assertStatus(200);
        $this->assertEquals($response->getContent(), '{"data":{"result":"\ud83d\ude42"}}');
    }
}

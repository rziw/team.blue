<?php

namespace App\Providers;

use App\Http\Dto\SampleDto;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(SampleDto::class, function () {
            return new SampleDto(['inputs' => request()->json()->all()]);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}

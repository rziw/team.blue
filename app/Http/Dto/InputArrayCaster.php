<?php
declare(strict_types = 1);

namespace App\Http\Dto;

use Spatie\DataTransferObject\Caster;

class InputArrayCaster implements Caster
{
    public function cast(mixed $inputs): array
    {
        if (count($inputs) > 2) {
            $randomInputs = [];
            $randomKeys = array_rand($inputs,2);

            foreach ($randomKeys as $key) {
                $randomInputs[] = $inputs[$key];
            }

            return $randomInputs;
        }

        return array_values($inputs);
    }
}

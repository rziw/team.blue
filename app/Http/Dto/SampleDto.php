<?php
declare(strict_types = 1);

namespace App\Http\Dto;

use App\Http\Validator\SampleValidator;
use Spatie\DataTransferObject\DataTransferObject;
use Spatie\DataTransferObject\Attributes\CastWith;

class SampleDto extends DataTransferObject
{
    #[SampleValidator]
    #[CastWith(InputArrayCaster::class)]
    public array $inputs;
}

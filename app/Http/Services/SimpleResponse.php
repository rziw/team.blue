<?php
declare(strict_types = 1);

namespace App\Http\Services;

use Illuminate\Http\JsonResponse;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Services\Interfaces\SampleResponseInterface;

class SimpleResponse implements SampleResponseInterface
{
    use ApiResponseTrait;

    public function getManipulatedData(array $data): JsonResponse
    {
        $result = reset($data);

        return $this->successResponse(['result' => $result]);
    }
}

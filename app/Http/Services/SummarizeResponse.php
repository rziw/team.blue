<?php
declare(strict_types = 1);

namespace App\Http\Services;

use Illuminate\Http\JsonResponse;
use App\Http\Traits\ApiResponseTrait;
use App\Http\Services\Interfaces\SampleResponseInterface;

class SummarizeResponse implements SampleResponseInterface
{
    use ApiResponseTrait;

    public function getManipulatedData(array $data): JsonResponse
    {
        $result = $data[0] + $data[1];

        return $this->successResponse(['result' => $result]);
    }
}

<?php
declare(strict_types = 1);

namespace App\Http\Services\Interfaces;

use Illuminate\Http\JsonResponse;

interface SampleResponseInterface
{
    public function getManipulatedData(array $data): JsonResponse;
}

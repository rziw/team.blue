<?php
declare(strict_types=1);

namespace App\Http\Services;

use App\Http\Services\Interfaces\SampleResponseInterface;
use Symfony\Component\HttpFoundation\Exception\BadRequestException;

class SampleResponseFactory
{
    public static function responseMethod(array $data): SampleResponseInterface
    {
        switch ($data) {
            case count($data) == 1 || self::haEmojis($data):
                return new SimpleResponse();
            case is_int($data[0]) && is_int($data[1]):
                return new SummarizeResponse();
            case is_string($data[0]) || is_string($data[1]):
                return new ConcatenationResponse();
            default:
                throw new BadRequestException("Unknown Condition");
        }
    }

    public static function haEmojis(array $data): bool
    {
        foreach ($data as $input) {
            if(is_string($input)) {
                $encodedString = rawurlencode($input);

                if(strpos($encodedString,"%F0") !== false) {
                    return true;
                }
            }
        }

        return false;
    }
}

<?php
declare(strict_types = 1);

namespace App\Http\Services;

use Illuminate\Http\JsonResponse;
use App\Http\Services\Interfaces\SampleResponseInterface;

class SampleContext
{
    private SampleResponseInterface $strategy;

    public function setStrategy(SampleResponseInterface $strategy): void
    {
        $this->strategy = $strategy;
    }

    public function getManipulatedData(array $data): JsonResponse
    {
        return $this->strategy->getManipulatedData($data);
    }
}

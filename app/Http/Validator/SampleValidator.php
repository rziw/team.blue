<?php
declare(strict_types = 1);

namespace App\Http\Validator;

use Attribute;
use Spatie\DataTransferObject\Validator;
use Spatie\DataTransferObject\Validation\ValidationResult;

#[Attribute(Attribute::TARGET_PROPERTY)]
class SampleValidator implements Validator
{
    public function validate(mixed $value): ValidationResult
    {
       if (count($value) === 0) {
           return ValidationResult::invalid("At least one parameter should be provided");
       }

        return ValidationResult::valid();
    }
}

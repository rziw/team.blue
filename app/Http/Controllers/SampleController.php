<?php
declare(strict_types = 1);

namespace App\Http\Controllers;

use App\Http\Dto\SampleDto;
use Illuminate\Http\JsonResponse;
use App\Http\Services\SampleContext;
use App\Http\Services\SampleResponseFactory;

class SampleController extends Controller
{
    public function getInput(SampleContext $sampleContext, SampleDto $sampleDto): JsonResponse
    {
        $sampleResponseStrategy = SampleResponseFactory::responseMethod($sampleDto->inputs);
        $sampleContext->setStrategy($sampleResponseStrategy);
        $result = $sampleContext->getManipulatedData($sampleDto->inputs);

        return $result;
    }
}

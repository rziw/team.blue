<?php
declare(strict_types = 1);

namespace App\Http\Traits;

use Illuminate\Http\JsonResponse;

trait ApiResponseTrait
{
    public function successResponse(array $data, int $statusCode = 200): JsonResponse
    {
        return response()->json([
            'data' => $data,
        ], $statusCode);
    }

    public function errorResponse(string $data, int $statusCode): JsonResponse
    {
        return response()->json([
            'error' => $data,
        ], $statusCode);
    }
}
